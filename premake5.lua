-- io.input("config.h.cmake")
-- local text = io.read("*a")

-- text = string.gsub(text, "@PLUGIN_API_VERSION_MAJOR@", "0")
-- text = string.gsub(text, "@PLUGIN_API_VERSION_MINOR@", "1")

-- text = string.gsub(text, "@AI_VERSION_MAJOR@", "0")
-- text = string.gsub(text, "@AI_VERSION_MINOR@", "1")

-- io.output("include/config.hpp")
-- io.write(text)
-- io.close()

local sdl2build = {}
local sdl2link  = {}

local sdl2tmp, err = os.outputof("pkg-config --cflags sdl2")
if err ~= 0 then
	print("Problem while running `pkg-config --cflags sdl2`")
end
for s in string.gmatch(sdl2tmp, "%S+") do
	table.insert(sdl2build, s)
end

sdl2tmp, err = os.outputof("pkg-config --libs sdl2")
if err ~= 0 then
	print("Problem while running `pkg-config --libs sdl2`")
end
for s in string.gmatch(sdl2tmp, "%S+") do
	table.insert(sdl2link, s)
end

solution("Chip8")
	configurations({"debug", "release"})
		buildoptions(
			{
				"-std=c++17"
			}
		)

		warnings "Extra"
		-- flags(
			-- {
				-- "ExtraWarnings"
			-- }
		-- )

	includedirs(
		{
			"include/"
		}
	)

	configuration("debug")
		buildoptions(
			{
				"-g"
			}
		)
		-- flags(
			-- {
				-- "Symbols"
			-- }
		-- )
		symbols "on"

	configuration("release")
		buildoptions(
			{
				"-O3"
			}
		)

	project("chip8")
		language("C++")
		kind("ConsoleApp")

		location("build/bin")
		targetdir("build/bin")

		files(
			{
				"src/*.cpp"
			}
		)

		buildoptions(sdl2build)
		linkoptions(sdl2link)

		-- links(
			-- {
				-- "PathFinder"
				-- , "Graph"
			-- }
		-- )
		-- libdirs(
			-- {
				-- "build/lib"
			-- }
		-- )
