#ifndef DEBUG_H_0ZY9XGZ4
#define DEBUG_H_0ZY9XGZ4

#include <functional>
#include <string>
#include <unordered_map>
#include <vector>

#include <SDL2/SDL.h>

#include "chip8.h"

enum class ByteHexStates {
	STATE_START_OF_LINE,	// print out the address.
	STATE_BYTES_1,		// Up to 8 bytes.
	STATE_MIDDLE_SEP,	// Middle separator.
	STATE_BYTES_2,		// Up to 8 bytes.
	STATE_END_SEP,		// End separator.
	STATE_CHARS,		// Printable characters.
	STATE_NEW_LINE,
};

void DebugCmdQuit(const char *cmd, const std::vector<std::string> &args, Chip8 *vm);
void DebugCmdBreakpoint(const char *cmd, const std::vector<std::string> &args, Chip8 *vm);

int DbgThreadFunc(void *data);

#endif /* end of include guard: DEBUG_H_0ZY9XGZ4 */
