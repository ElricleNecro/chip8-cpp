#ifndef CHIP8_H_KTEN0D8B
#define CHIP8_H_KTEN0D8B

#include <cstdio>
#include <cstdint>
#include <cstring>

#include <algorithm>
#include <memory>
#include <stack>
#include <unordered_set>
#include <vector>

#include <SDL2/SDL.h>

const int PIXEL_SIZE = 16;
const unsigned int VM_MEM_SIZE = 4096;

class Chip8 {
	public:
		Chip8(void);
		~Chip8(void);

		bool read_memory_image(const char *fname);

		void reset(void);

		void run(void);

		void setScreenBuffer(uint8_t *buf, uint32_t w, uint32_t h);
		void redraw(void);

		void disassembler(uint16_t addr, uint16_t op);

		// TODO: the framebuffer should be in memory?
		uint8_t fb[64 * 32]{};
		uint8_t mem[VM_MEM_SIZE]{};
		uint8_t v[16]{0};
		uint16_t i{};
		uint16_t pc{};

		uint8_t delay_timer{};
		uint8_t sound_timer{};

		bool key_pressed[0x10]{}; // 0-9 A-F
		int last_key_pressed{};

		// Debugguer variable:
		std::unordered_set<int> debug_breakpoints;
		SDL_mutex *debug_breakpoints_mutex;
		bool debug_break_execution;

		SDL_atomic_t the_end{};

	private:
		struct OpcodeTableEntry {
			uint16_t opcode;
			uint16_t mask;

			void (Chip8::*handler)(uint16_t);

			std::string mnemonic;
			std::string args;
		};

		std::vector<OpcodeTableEntry> opcode_table;
		uint8_t *screen{};
		uint32_t screen_w{}, screen_h{};

		// TODO: the stack should be in memory:
		std::stack<uint16_t> call_stack;

		void init_opcode(void);
		void draw_pixel(int x, int y, int color);

		uint16_t argNNN(uint16_t opcode) const;
		uint16_t argNN(uint16_t opcode) const;
		uint16_t argN(uint16_t opcode) const;
		uint16_t argX(uint16_t opcode) const;
		uint16_t argY(uint16_t opcode) const;

		void opcode0NNN(uint16_t opcode);
		void opcode00E0(uint16_t opcode);
		void opcode00EE(uint16_t opcode);
		void opcode1NNN(uint16_t opcode);
		void opcode2NNN(uint16_t opcode);
		void opcode3XNN(uint16_t opcode);
		void opcode4XNN(uint16_t opcode);
		void opcode5XY0(uint16_t opcode);
		void opcode6XNN(uint16_t opcode);
		void opcode7XNN(uint16_t opcode);
		void opcode8XY0(uint16_t opcode);
		void opcode8XY1(uint16_t opcode);
		void opcode8XY2(uint16_t opcode);
		void opcode8XY3(uint16_t opcode);
		void opcode8XY4(uint16_t opcode);
		void opcode8XY5(uint16_t opcode);
		void opcode8XY6(uint16_t opcode);
		void opcode8XY7(uint16_t opcode);
		void opcode8XYE(uint16_t opcode);
		void opcode9XY0(uint16_t opcode);
		void opcodeANNN(uint16_t opcode);
		void opcodeBNNN(uint16_t opcode);
		void opcodeCXNN(uint16_t opcode);
		void opcodeDXYN(uint16_t opcode);
		void opcodeEX9E(uint16_t opcode);
		void opcodeEXA1(uint16_t opcode);
		void opcodeFX07(uint16_t opcode);
		void opcodeFX0A(uint16_t opcode);
		void opcodeFX15(uint16_t opcode);
		void opcodeFX18(uint16_t opcode);
		void opcodeFX1E(uint16_t opcode);
		void opcodeFX29(uint16_t opcode);
		void opcodeFX33(uint16_t opcode);
		void opcodeFX55(uint16_t opcode);
		void opcodeFX65(uint16_t opcode);
};

#endif /* end of include guard: CHIP8_H_KTEN0D8B */
