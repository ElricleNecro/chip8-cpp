#include "chip8.h"

struct FileDeleter {
	void operator()(FILE *ptr) const {
		fclose(ptr);
	}
};

Chip8::Chip8(void) {
	this->init_opcode();
	this->reset();

	this->debug_breakpoints_mutex = SDL_CreateMutex();
	this->debug_break_execution = true;
}

Chip8::~Chip8(void) {
	SDL_DestroyMutex(this->debug_breakpoints_mutex);
}

bool Chip8::read_memory_image(const char *fname) {
	std::unique_ptr<FILE, FileDeleter> f;
	f.reset(fopen(fname, "rb"));

	if( f == nullptr ) {
		return false;
	}

	return fread(this->mem + 512, 1, 4096 - 512, f.get()) > 0;
}

void Chip8::reset(void) {
	memset(this->mem, 0, sizeof(this->mem));

	for(int i = 0; i < 16; i++) {
		this->v[i] = 0;
	}

	this->i = 0;
	this->pc = 0x200;

	this->delay_timer = 0;
	this->sound_timer = 0;

	this->last_key_pressed = -1;
	for(int i=0; i < 0x10; i++) {
		this->key_pressed[i] = false;
	}
}

void Chip8::run(void) {
	uint32_t last_ticks = SDL_GetTicks();

	for(;;) {
		if( SDL_AtomicGet(&this->the_end) != 0 ) {
			break;
		}

		// Handle timer:
		uint32_t now = SDL_GetTicks();
		if( now - last_ticks > 16 ) {
			uint32_t diff = now - last_ticks;
			uint32_t timer_ticks = diff / 16;

			this->delay_timer = std::max(0, (int)this->delay_timer - (int)timer_ticks);
			this->sound_timer = std::max(0, (int)this->sound_timer - (int)timer_ticks);

			last_ticks = now - diff % 16; // Take into account "unused time".
		}

		// Execute the instruction:
		uint16_t opcode;
		if( this->pc + 1 >= 4096) {
			printf("Errors: pc out of bounds: %.4x.\n", this->pc);
			return ;
		}

		SDL_LockMutex(this->debug_breakpoints_mutex);
		if( this->debug_breakpoints.find(this->pc) != this->debug_breakpoints.end() ) {
			puts("Hit breakpoint.");
			this->debug_break_execution = true;
		}
		SDL_UnlockMutex(this->debug_breakpoints_mutex);

		while( this->debug_break_execution ) {
			SDL_Delay(10);
			if( SDL_AtomicGet(&this->the_end) != 0 ) {
				return ;
			}
		}

		// Fetch big-endian word from memory:
		opcode = this->mem[this->pc] << 8; // BigEndian
		opcode |= this->mem[this->pc + 1];
		this->pc += 2;

		for(const auto& entry: this->opcode_table) {
			if( (opcode & entry.mask) == entry.opcode ) {
				(this->*entry.handler)(opcode);
				break;
			}
		}

		SDL_Delay(1);
	}
}

void Chip8::init_opcode(void) {
	this->opcode_table = std::vector<OpcodeTableEntry>{
		{ /* 0x00E0 */ 0x00E0, 0xFFFF, &Chip8::opcode00E0, "CLS",  ""    },
		{ /* 0x00EE */ 0x00EE, 0xFFFF, &Chip8::opcode00EE, "RET",  ""    },
		{ /* 0x0NNN */ 0x0000, 0xF000, &Chip8::opcode0NNN, "SYS",  "NNN" },
		{ /* 0x1NNN */ 0x1000, 0xF000, &Chip8::opcode1NNN, "JP",   "NNN" },
		{ /* 0x2NNN */ 0x2000, 0xF000, &Chip8::opcode2NNN, "CALL", "NNN" },
		{ /* 0x3XNN */ 0x3000, 0xF000, &Chip8::opcode3XNN, "SE",   "XNN" },
		{ /* 0x4XNN */ 0x4000, 0xF000, &Chip8::opcode4XNN, "SNE",  "XNN" },
		{ /* 0x5XY0 */ 0x5000, 0xF00F, &Chip8::opcode5XY0, "SE",   "XY"  },
		{ /* 0x6XNN */ 0x6000, 0xF000, &Chip8::opcode6XNN, "LD",   "XNN" },
		{ /* 0x7XNN */ 0x7000, 0xF000, &Chip8::opcode7XNN, "ADD",  "XNN" },
		{ /* 0x8XY0 */ 0x8000, 0xF00F, &Chip8::opcode8XY0, "LD",   "XY"  },
		{ /* 0x8XY1 */ 0x8001, 0xF00F, &Chip8::opcode8XY1, "OR",   "XY"  },
		{ /* 0x8XY2 */ 0x8002, 0xF00F, &Chip8::opcode8XY2, "AND",  "XY"  },
		{ /* 0x8XY3 */ 0x8003, 0xF00F, &Chip8::opcode8XY3, "XOR",  "XY"  },
		{ /* 0x8XY4 */ 0x8004, 0xF00F, &Chip8::opcode8XY4, "ADD",  "XY"  },
		{ /* 0x8XY5 */ 0x8005, 0xF00F, &Chip8::opcode8XY5, "SUB",  "XY"  },
		{ /* 0x8XY6 */ 0x8006, 0xF00F, &Chip8::opcode8XY6, "SHR",  "XY"  },
		{ /* 0x8XY7 */ 0x8007, 0xF00F, &Chip8::opcode8XY7, "SUBN", "XY"  },
		{ /* 0x8XYE */ 0x800E, 0xF00F, &Chip8::opcode8XYE, "SHL",  "XY"  },
		{ /* 0x9XY0 */ 0x9000, 0xF00F, &Chip8::opcode9XY0, "SNE",  "XY"  },
		{ /* 0xANNN */ 0xA000, 0xF000, &Chip8::opcodeANNN, "LD",   "NNN" },
		{ /* 0xBNNN */ 0xB000, 0xF000, &Chip8::opcodeBNNN, "JP",   "NNN" },
		{ /* 0xCXNN */ 0xC000, 0xF000, &Chip8::opcodeCXNN, "RND",  "XNN" },
		{ /* 0xDXYN */ 0xD000, 0xF000, &Chip8::opcodeDXYN, "DRW",  "XYN" },
		{ /* 0xEX9E */ 0xE09E, 0xF0FF, &Chip8::opcodeEX9E, "SKP",  "X"   },
		{ /* 0xEXA1 */ 0xE0A1, 0xF0FF, &Chip8::opcodeEXA1, "SKNP", "X"   },
		{ /* 0xFX07 */ 0xF007, 0xF0FF, &Chip8::opcodeFX07, "LD",   "X"   },
		{ /* 0xFX0A */ 0xF00A, 0xF0FF, &Chip8::opcodeFX0A, "LD",   "X"   },
		{ /* 0xFX15 */ 0xF015, 0xF0FF, &Chip8::opcodeFX15, "LD",   "X"   },
		{ /* 0xFX18 */ 0xF018, 0xF0FF, &Chip8::opcodeFX18, "LD",   "X"   },
		{ /* 0xFX1E */ 0xF01E, 0xF0FF, &Chip8::opcodeFX1E, "ADD",  "X"   },
		{ /* 0xFX29 */ 0xF029, 0xF0FF, &Chip8::opcodeFX29, "LD",   "X"   },
		{ /* 0xFX33 */ 0xF033, 0xF0FF, &Chip8::opcodeFX33, "LD",   "X"   },
		{ /* 0xFX55 */ 0xF055, 0xF0FF, &Chip8::opcodeFX55, "LD",   "X"   },
		{ /* 0xFX65 */ 0xF065, 0xF0FF, &Chip8::opcodeFX65, "LD",   "X"   },
	};
}

void Chip8::setScreenBuffer(uint8_t *buf, uint32_t w, uint32_t h) {
	this->screen   = buf;
	this->screen_w = w;
	this->screen_h = h;
}

void Chip8::draw_pixel(int x, int y, int color) {
	// if( x < 0 || x >= 64 || y < 0 || y >= 32 ) {
		// return ;
	// }

	// printf("(%d, %d)\n", x, y);

	for (int k = y * PIXEL_SIZE; k < (y + 1) * PIXEL_SIZE; k++) {
		for (int j = x * PIXEL_SIZE; j < (x + 1) * PIXEL_SIZE; j++) {
			this->screen[(k * this->screen_w + j) * 4 + 0] = color;
			this->screen[(k * this->screen_w + j) * 4 + 1] = color;
			this->screen[(k * this->screen_w + j) * 4 + 2] = color;
			this->screen[(k * this->screen_w + j) * 4 + 3] = 0xff;
		}
	}
}

void Chip8::redraw(void) {
	if( this->screen == nullptr )
		return;

	for (int y = 0; y < 32; y++) {
		for (int x = 0; x < 64; x++) {
			this->draw_pixel(x, y, this->fb[x + y * 64] * 0xff);
		}
	}
}

void Chip8::disassembler(uint16_t addr, uint16_t op) {
	for(const auto& entry: this->opcode_table) {
		if( (op & entry.mask) == entry.opcode ) {
			printf("%.4x: %s ", addr, entry.mnemonic.c_str());

			if( entry.args.empty() ) {
				return ;
			}

			if( entry.args == "NNN" ) {
				if( (op & 0xf000) == 0xa000 ) {
					printf("I, ");
				} else if( (op & 0xf000) == 0xb000 ) {
					printf("PC, ");
				}

				printf("0x%.3x\n", this->argNNN(op));
				return ;
			}

			if( entry.args == "XNN" ) {
				printf("V%i, 0x%.2x\n", this->argX(op), this->argNN(op));
				return ;
			}

			if( entry.args == "XY" ) {
				printf("V%i, V%i\n", this->argX(op), this->argY(op));
				return ;
			}

			if( entry.args == "XYN" ) {
				printf("V%i, V%i, 0x%.1x\n", this->argX(op), this->argY(op), this->argN(op));
				return ;
			}

			if( entry.args == "X" ) {
				if( (op & entry.mask) == 0xF007 ) {
					printf("V%i, Delay\n", this->argX(op));
				} else if( (op & entry.mask) == 0xF015 ) {
					printf("Delay, V%i\n", this->argX(op));
				} else if( (op & entry.mask) == 0xF018 ) {
					printf("Sound, V%i\n", this->argX(op));
				}
				return ;
			}

			puts("???");

			return ;
		}
	}

	printf("%.4x: ???\n", addr);
}

uint16_t Chip8::argNNN(const uint16_t opcode) const {
	return opcode & 0x0fff;
}

uint16_t Chip8::argNN(const uint16_t opcode) const {
	return opcode & 0x00ff;
}

uint16_t Chip8::argN(const uint16_t opcode) const {
	return opcode & 0x000f;
}

uint16_t Chip8::argX(const uint16_t opcode) const {
	return (opcode & 0x0f00) >> 8;
}

uint16_t Chip8::argY(const uint16_t opcode) const {
	return (opcode & 0x00f0) >> 4;
}

void Chip8::opcode0NNN(uint16_t) {
	printf("pc=%.4x: opcode 0NNN is not implemented.\n", this->pc);
}

void Chip8::opcode00E0(uint16_t) {
	memset(this->fb, 0, 64 * 32);
	this->redraw();
}

void Chip8::opcode00EE(uint16_t) {
	if( this->call_stack.empty() ) {
		// TODO: what to do...
		printf("pc=%.4x: stack empty, but return used?\n", this->pc);
		return ;
	}

	this->pc = this->call_stack.top();
	this->call_stack.pop();
}

void Chip8::opcode1NNN(uint16_t opcode) {
	this->pc = this->argNNN(opcode);
}

void Chip8::opcode2NNN(uint16_t opcode) {
	this->call_stack.push(this->pc);
	this->pc = this->argNNN(opcode);
	// TODO: Should we have a stack limit?
}

void Chip8::opcode3XNN(uint16_t opcode) {
	if( this->v[this->argX(opcode)] == this->argNN(opcode) )
		this->pc += 2;
}

void Chip8::opcode4XNN(uint16_t opcode) {
	if( this->v[this->argX(opcode)] != this->argNN(opcode) )
		this->pc += 2;
}

void Chip8::opcode5XY0(uint16_t opcode) {
	if( this->v[this->argX(opcode)] == this->v[this->argY(opcode)] )
		this->pc += 2;
}

void Chip8::opcode6XNN(uint16_t opcode) {
	this->v[this->argX(opcode)] = this->argNN(opcode);
}

void Chip8::opcode7XNN(uint16_t opcode) {
	this->v[this->argX(opcode)] += this->argNN(opcode);
}

void Chip8::opcode8XY0(uint16_t opcode) {
	this->v[this->argX(opcode)] = this->v[this->argY(opcode)];
}

void Chip8::opcode8XY1(uint16_t opcode) {
	this->v[this->argX(opcode)] |= this->v[this->argY(opcode)];
}

void Chip8::opcode8XY2(uint16_t opcode) {
	this->v[this->argX(opcode)] &= this->v[this->argY(opcode)];
}

void Chip8::opcode8XY3(uint16_t opcode) {
	this->v[this->argX(opcode)] ^= this->v[this->argY(opcode)];
}

void Chip8::opcode8XY4(uint16_t opcode) {
	uint16_t res = (uint16_t)this->v[this->argX(opcode)] + (uint16_t)this->v[this->argY(opcode)];

	this->v[this->argX(opcode)] = (uint8_t)res;
	this->v[0xf] = (res >= 0x100);
}

void Chip8::opcode8XY5(uint16_t opcode) {
	int16_t res = (int16_t)this->v[this->argX(opcode)] - (int16_t)this->v[this->argY(opcode)];

	this->v[this->argX(opcode)] = (uint8_t)res;
	this->v[0xf] = (res >= 0);
}

void Chip8::opcode8XY6(uint16_t opcode) {
	uint8_t lsb = this->v[this->argY(opcode)] & 1;
	uint8_t res = this->v[this->argY(opcode)] >> 1;

	this->v[this->argY(opcode)] = res;
	this->v[this->argX(opcode)] = res;
	this->v[0xf]                = lsb;
}

void Chip8::opcode8XY7(uint16_t opcode) {
	int16_t res = (int16_t)this->v[this->argY(opcode)] - (int16_t)this->v[this->argX(opcode)];

	this->v[this->argX(opcode)] = (uint8_t)res;
	this->v[0xf] = (res >= 0);
}

void Chip8::opcode8XYE(uint16_t opcode) {
	uint8_t msb = this->v[this->argY(opcode)] >> 7;
	uint8_t res = this->v[this->argY(opcode)] << 1;

	this->v[this->argY(opcode)] = res;
	this->v[this->argX(opcode)] = res;
	this->v[0xf]                = msb;
}

void Chip8::opcode9XY0(uint16_t opcode) {
	if( this->v[this->argX(opcode)] != this->v[this->argY(opcode)] )
		this->pc += 2;
}

void Chip8::opcodeANNN(uint16_t opcode) {
	this->i = this->argNNN(opcode);
}

void Chip8::opcodeBNNN(uint16_t opcode) {
	this->pc = (this->v[0] + this->argNNN(opcode)) & 0xfff;
}

void Chip8::opcodeCXNN(uint16_t opcode) {
	this->v[this->argX(opcode)] = rand() & this->argNN(opcode);
}

void Chip8::opcodeDXYN(uint16_t opcode) {
	uint8_t x = this->v[this->argX(opcode)] % 64;
	uint8_t y = this->v[this->argY(opcode)] % 32;
	uint8_t n = this->argN(opcode);
	bool flipped = false;

	for(int j=0; j < n; j++, y++) {
		uint8_t px = this->mem[this->i + j];
		for(int k=0; k < 8; k++) {
			uint8_t bit = ((px >> (7 - k)) & 1);
			if( bit && this->fb[x + k + y * 64] )
				flipped |= true;

			this->fb[x + k + y * 64] ^= bit;
		}
	}

	this->v[0xf] = flipped;

	this->redraw();
}

void Chip8::opcodeEX9E(uint16_t opcode) {
	int idx = this->v[this->argX(opcode)];
	bool pressed = false;
	if( idx < 0x10 ) {
		pressed = this->key_pressed[idx];
	}

	if( pressed )
		this->pc += 2;
}

void Chip8::opcodeEXA1(uint16_t opcode) {
	int idx = this->v[this->argX(opcode)];
	bool pressed = false;
	if( idx < 0x10 ) {
		pressed = this->key_pressed[idx];
	}

	if( ! pressed )
		this->pc += 2;
}

void Chip8::opcodeFX07(uint16_t opcode) {
	this->v[this->argX(opcode)] = this->delay_timer;
}

void Chip8::opcodeFX0A(uint16_t opcode) {
	int k;
	// TODO: deal with the potential race condition in this loop.
	while( (k = this->last_key_pressed) == -1 ) {
		SDL_Delay(5);
	}

	this->v[this->argX(opcode)] = k;
}

void Chip8::opcodeFX15(uint16_t opcode) {
	this->delay_timer = this->v[this->argX(opcode)];
}

void Chip8::opcodeFX18(uint16_t opcode) {
	this->sound_timer = this->v[this->argX(opcode)];
}

void Chip8::opcodeFX1E(uint16_t opcode) {
	this->i = (this->i + this->v[this->argX(opcode)]) & 0xfff;
}

void Chip8::opcodeFX29(uint16_t opcode) {
}

void Chip8::opcodeFX33(uint16_t opcode) {
	uint16_t bcd = this->v[this->argX(opcode)];

	this->mem[this->i + 0] = bcd / 100;
	this->mem[this->i + 1] = (bcd / 10) % 10;
	this->mem[this->i + 2] = bcd % 10;
}

void Chip8::opcodeFX55(uint16_t opcode) {
	unsigned int last_reg = this->argX(opcode);
	for (unsigned int j = 0; j < last_reg; j++) {
		this->mem[this->i] = this->v[j];
		this->i += 1;
	}
}

void Chip8::opcodeFX65(uint16_t opcode) {
	unsigned int last_reg = this->argX(opcode);
	for (unsigned int j = 0; j < last_reg; j++) {
		this->v[j] = this->mem[this->i];
		this->i += 1;
	}
}
