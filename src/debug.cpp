#include "debug.h"

void DebugCmdQuit(const char*, const std::vector<std::string>&, Chip8 *vm) {
	SDL_AtomicSet(&vm->the_end, 1);
}

void DebugCmdRun(const char*, const std::vector<std::string> &, Chip8 *vm) {
	vm->debug_break_execution = false;
}

void DebugCmdRegisters(const char*, const std::vector<std::string> &, Chip8 *vm) {
	for(int i=0; i < 16; i++) {
		char reg_name[8]{};
		sprintf(reg_name, "V%i", i);
		printf("%3s: %.2x    ", reg_name, vm->v[i]);
		if( i % 4 == 3 )
			puts("");
	}
	printf("  I: %.4x\n", vm->i);
	printf(" pc: %.4x\n", vm->pc);
}

void DebugCmdReadMemory(const char *cmd, const std::vector<std::string> &args, Chip8 *vm) {
	if( args.size() != 3 ) {
		printf("Usage: %s <type> <adresse_hex> <count/dec>\n", cmd);
		puts("types: b (byte), w (word)");
		return ;
	}

	if( args[0] != "b" && args[0] != "w" && args[0] != "c" ) {
		printf("Error: '%s' is not a valid type.", args[0].c_str());
		return ;
	}

	unsigned int addr = 0;
	if( sscanf(args[1].c_str(), "%x", &addr) != 1 || addr >= VM_MEM_SIZE ) {
		printf("Error: '%s' is not a valid address.", args[1].c_str());
	}

	int count = 0;
	if( sscanf(args[2].c_str(), "%i", &count) != 1 || count < 0 || (unsigned int)count > VM_MEM_SIZE || (addr + count) >= VM_MEM_SIZE ) {
		printf("Error: '%s' is not a valid offset.", args[2].c_str());
	}

	if( args[0] == "b") {
		ByteHexStates state = ByteHexStates::STATE_START_OF_LINE;

						// Used in states:
		int i = 0;			// STATE_BYTES_1, STATE_BYTES_2
		unsigned int addr_hex  = addr;	// STATE_BYTES_1, STATE_BYTES_2
		unsigned int addr_char = addr;	// STATE_CHARS
		unsigned int end_addr  = addr + (unsigned int)count;

		bool done = false;
		while( ! done ) {
			switch(state) {
				case ByteHexStates::STATE_START_OF_LINE:
					printf("%.4x:", addr_hex);
					state = ByteHexStates::STATE_BYTES_1;
					i = 0;
					continue;

				case ByteHexStates::STATE_BYTES_1:
				case ByteHexStates::STATE_BYTES_2:
					if( addr_hex < end_addr ) {
						printf(" %.2x", vm->mem[addr_hex]);
						addr_hex++;
					} else {
						printf("   ");
					}

					i++;
					if( i == 8 ) {
						if( state == ByteHexStates::STATE_BYTES_1 ) {
							state = ByteHexStates::STATE_MIDDLE_SEP;
						} else {
							state = ByteHexStates::STATE_END_SEP;
						}
					}

					continue;

				case ByteHexStates::STATE_MIDDLE_SEP:
					putchar(' ');
					i = 0;
					state = ByteHexStates::STATE_BYTES_2;
					continue;

				case ByteHexStates::STATE_END_SEP:
					printf(" | ");
					i = 0;
					state = ByteHexStates::STATE_CHARS;
					continue;

				case ByteHexStates::STATE_CHARS:
					if( addr_char < end_addr ) {
						uint8_t b = vm->mem[addr_char];
						if( isprint(b) ) {
							putchar((int)b);
						} else {
							putchar('.');
						}
						addr_char++;
					} else {
						putchar(' ');
					}

					i++;
					if( i == 16 ) {
						state = ByteHexStates::STATE_NEW_LINE;
					}

					continue;

				case ByteHexStates::STATE_NEW_LINE:
					putchar('\n');
					if( addr_hex >= end_addr || addr_char >= end_addr ) {
						done = true;
					}
					i = 0;
					state = ByteHexStates::STATE_START_OF_LINE;
					continue;
			}
		}
	}

	if( args[0] == "w" ) {
		int i;
		for(i = 0; i < count; i++) {
			if( (i % 8) == 0 )
				printf("%.4x:", addr + i * 2);

			uint8_t b0 = vm->mem[addr + i * 2];
			uint8_t b1 = vm->mem[addr + i * 2 + 1];
			uint16_t w = (b0 << 8) | b1;

			printf(" %.4x", w);

			if( (i + 1) == count || ((i + 1) % 8) == 0 )
				putchar('\n');
		}
	}
}

void DebugCmdBreakpoint(const char*, const std::vector<std::string> &args, Chip8 *vm) {
	if( args.size() != 1 ) {
		printf("Error: Wrong argument count, expected 1 argument (adress) but %lu were given.\n", args.size());
		return ;
	}

	int addr = 0;
	if( sscanf(args[0].c_str(), "%x", &addr) != 1 ) {
		printf("Error: Wrong argument format; should be an address.\n");
		return ;
	}

	SDL_LockMutex(vm->debug_breakpoints_mutex);
	vm->debug_breakpoints.insert(addr);
	SDL_UnlockMutex(vm->debug_breakpoints_mutex);

	printf("Breakpoint at 0x%x set.\n", addr);
}

void DebugCmdDissamble(const char *cmd, const std::vector<std::string> &args, Chip8 *vm) {
	if( args.size() != 1 && args.size() != 2 ) {
		printf("Usage: %s <address_hex> [count]\n", cmd);
		return ;
	}

	unsigned int addr = 0;
	if( sscanf(args[0].c_str(), "%x", &addr) != 1 || addr >= VM_MEM_SIZE ) {
		printf("Error: '%s' is not a valid address.", args[1].c_str());
	}

	int count = 10;
	if( args.size() == 2 ) {
		if( sscanf(args[1].c_str(), "%i", &count) != 1 ) {
			printf("Error: '%s' is not a correct count.", args[1].c_str());
			return ;
		}
	}

	for(int i = 0; i < count && addr + 1 < VM_MEM_SIZE; i++, addr += 2) {
		uint8_t b0 = vm->mem[addr];
		uint8_t b1 = vm->mem[addr + 1];
		uint16_t w = (b0 << 8) | b1;

		vm->disassembler(addr, w);
	}
}

int DbgThreadFunc(void *data) {
	Chip8 *vm = (Chip8*)data;

	std::unordered_map<std::string, std::function<void(const char*, const std::vector<std::string>&, Chip8*)>> debug_cmds = {
		{ "b", DebugCmdBreakpoint },
		{ "break", DebugCmdBreakpoint },
		{ "q", DebugCmdQuit },
		{ "quit", DebugCmdQuit },
		{ "exit", DebugCmdQuit },
		{ "bye", DebugCmdQuit },
		{ "run", DebugCmdRun },
		{ "start", DebugCmdRun },
		{ "go", DebugCmdRun },
		{ "g", DebugCmdRun },
		{ "r", DebugCmdRegisters },
		{ "reg", DebugCmdRegisters },
		{ "regs", DebugCmdRegisters },
		{ "registers", DebugCmdRegisters },
		{ "x", DebugCmdReadMemory },
		{ "mem", DebugCmdReadMemory },
		{ "memory", DebugCmdReadMemory },
		{ "dis", DebugCmdDissamble },
		{ "disas", DebugCmdDissamble },
		{ "disasm", DebugCmdDissamble },
		{ "disassemble", DebugCmdDissamble },
	};

	for(;;) {
		if( SDL_AtomicGet(&vm->the_end) != 0 ) {
			break;
		}

		printf("\033[1;31mchip8dbg> \033[00m");
		fflush(stdout);

		char line[2048]{};
		if( scanf(" %2047[^\n]s", line) != 1 ) {
			continue;
		}

		char *p = line;
		char cmd[2048]{};
		int cmd_size{};

		sscanf(line, "%2047s%n", cmd, &cmd_size);
		p += cmd_size;

		std::vector<std::string> args;
		for(;;) {
			char arg[2048]{};
			int arg_size{};
			if( sscanf(p, " %2047s%n", arg, &arg_size) != 1 ) {
				break;
			}

			args.emplace_back(std::string(arg));
			p += arg_size;
		}

		const auto func = debug_cmds.find(cmd);
		if( func == debug_cmds.end() ) {
			printf("Command \"%s\" not found.\n", cmd);
			continue;
		}

		func->second(cmd, args, vm);
	}

	return 0;
}

