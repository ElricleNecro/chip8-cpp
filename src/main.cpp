#include <cstdio>
#include <cstdlib>
#include <iostream>

#include <SDL2/SDL.h>

#include "chip8.h"
#include "debug.h"

#define EXIT_FAILURE_ARGS                1
#define EXIT_FAILURE_READ_ROM            2
#define EXIT_FAILURE_SDL                 3
#define EXIT_FAILURE_SDL_CREATE_WINDOW   4
#define EXIT_FAILURE_SDL_CREATE_RENDERER 5

int VMThreadFunc(void *data) {
	Chip8 *vm = (Chip8*)data;

	vm->run();

	return 0;
}

int translateCodeToIndex(SDL_Scancode key) {
	switch(key) {
		case SDL_SCANCODE_0:
			return 0;

		case SDL_SCANCODE_1:
			return 1;

		case SDL_SCANCODE_2:
			return 2;

		case SDL_SCANCODE_3:
			return 3;

		case SDL_SCANCODE_4:
			return 4;

		case SDL_SCANCODE_5:
			return 5;

		case SDL_SCANCODE_6:
			return 6;

		case SDL_SCANCODE_7:
			return 7;

		case SDL_SCANCODE_8:
			return 8;

		case SDL_SCANCODE_9:
			return 9;

		case SDL_SCANCODE_KP_0:
			return 0;

		case SDL_SCANCODE_KP_1:
			return 1;

		case SDL_SCANCODE_KP_2:
			return 2;

		case SDL_SCANCODE_KP_3:
			return 3;

		case SDL_SCANCODE_KP_4:
			return 4;

		case SDL_SCANCODE_KP_5:
			return 5;

		case SDL_SCANCODE_KP_6:
			return 6;

		case SDL_SCANCODE_KP_7:
			return 7;

		case SDL_SCANCODE_KP_8:
			return 8;

		case SDL_SCANCODE_KP_9:
			return 9;

		// QWERTY-AZERTY shit from SDL.
		case SDL_SCANCODE_Q:
			return 10;

		case SDL_SCANCODE_B:
			return 11;

		case SDL_SCANCODE_C:
			return 12;

		case SDL_SCANCODE_D:
			return 13;

		case SDL_SCANCODE_E:
			return 14;

		case SDL_SCANCODE_F:
			return 15;

		case SDL_SCANCODE_DOWN:
			return 8;

		case SDL_SCANCODE_UP:
			return 2;

		case SDL_SCANCODE_LEFT:
			return 4;

		case SDL_SCANCODE_RIGHT:
			return 6;

		default:
			return -1;
	}
}

int main(int argc, char *argv[]) {
	if( argc != 2 ) {
		puts("Usage: chip8 <filename>");
		return EXIT_FAILURE_ARGS;
	}

	auto vm = std::make_unique<Chip8>();
	if( ! vm->read_memory_image(argv[1]) ) {
		puts("Failed to read RAM image.");
		return EXIT_FAILURE_READ_ROM;
	}

	if( SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO | SDL_INIT_TIMER | SDL_INIT_EVENTS) != 0 ) {
		std::cout << "SDL_Init error: " << SDL_GetError() << std::endl;
		return EXIT_FAILURE_SDL;
	}

	SDL_Window *win = SDL_CreateWindow(
		"CHIP8",
		SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
		64 * PIXEL_SIZE, 32 * PIXEL_SIZE,
		SDL_WINDOW_SHOWN
	);
	if( win == nullptr ) {
		std::cout << "SDL_CreateWindow failed: " << SDL_GetError() << std::endl;
		SDL_Quit();
		return EXIT_FAILURE_SDL_CREATE_WINDOW;
	}

	SDL_Surface *surface    = SDL_GetWindowSurface(win);
	if( surface == nullptr ) {
		std::cout << "SDL_GetWindowSurface failed: " << SDL_GetError() << std::endl;
		SDL_DestroyWindow(win);
		SDL_Quit();
		return EXIT_FAILURE_SDL_CREATE_RENDERER;
	}
	vm->setScreenBuffer((uint8_t*)surface->pixels, 64 * PIXEL_SIZE, 32 * PIXEL_SIZE);

	SDL_Thread  *vm_thread  = SDL_CreateThread(VMThreadFunc, "vm-thread", vm.get());
	SDL_Thread  *dbg_thread = SDL_CreateThread(DbgThreadFunc, "dbg-thread", vm.get());

	SDL_Event e;
	bool the_end = false;
	while( ! the_end && SDL_AtomicGet(&vm->the_end) == 0 ) {
		while( SDL_PollEvent(&e) ) {
			switch(e.type) {
				case SDL_QUIT:
					the_end = true;
					break;

				case SDL_KEYUP:
				case SDL_KEYDOWN:
					int idx = translateCodeToIndex(e.key.keysym.scancode);
					if( idx != -1 ) {
						vm->key_pressed[idx] = e.key.state == SDL_PRESSED;
						if( e.key.state == SDL_PRESSED ) {
							vm->last_key_pressed = idx;
						} else {
							bool all_keys_released = true;
							for(int i=0; i < 0x10; i++) {
								if( vm->key_pressed[idx] ) {
									all_keys_released = false;
									break;
								}
							}

							if( all_keys_released )
								vm->last_key_pressed = -1;
						}
					}
					break;
			}
		}

		SDL_UpdateWindowSurface(win);
	}

	SDL_AtomicSet(&vm->the_end, 1);
	SDL_WaitThread(vm_thread, nullptr);
	SDL_WaitThread(dbg_thread, nullptr);

	SDL_DestroyWindow(win);
	SDL_Quit();

	return EXIT_SUCCESS;
}
